Erstellen Sie beliebig viele Aufgabenlisten und organisieren Sie deren
dynamische Abarbeitung.  Weisen Sie einzelnen Aufgaben Personen und Kategorien
zu, tauschen Sie sich über den aktuellen Fortschritt aus und verknüpfen Sie
Dokumente Ihrer Cloud mit verschiedenen Tasks.
