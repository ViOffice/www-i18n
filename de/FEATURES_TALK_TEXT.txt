Ob im Home Office oder von Unterwegs, Einzel- und Gruppenchats innerhalb der
eigenen Organisation, nutzbar sowohl auf Android/iOS als auch direkt in der
Cloud.
