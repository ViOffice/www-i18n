# Copyright (c) 2020 Weymeirsch und Langer GbR <infoATviofficeDOTde>
# Author 2020 Jan Weymeirsch <weymeirschATviofficeDOTde>
# SPDX-License-Identifier: GPL-3.0

# Catch Input ------------------------------------------------------------------
if [[ -z "$3" ]]; then
    echo "USAGE:"
    echo "./gen_html \"../index.tt\" \"../en/index.html\" \"./en\""
    exit 1;
fi
TEMPLATE="$1"
TARGET="$2"
LANGDIR="$3"
VARS=$(grep "<%.*%>" "$TEMPLATE" | sed -e 's/^.*<%//' -e 's/%>.*$//')

# Checks -----------------------------------------------------------------------

# Does Language directory exist?
if [[ ! -d "$LANGDIR" ]]; then
    echo "The given language-directory \"$LANGDIR\" does not exist!"
    exit 1;
fi

# Do all required variables exist?
for v in ${VARS}; do
    if [[ ! -f "$LANGDIR/$v.txt" ]]; then
        echo "The required translation in \"$LANGDIR/$v.txt\" does not exist!"
        exit 1;
    fi
done

# Build HTML -------------------------------------------------------------------

# Generate backup if necessary
if [[ -f "$TARGET" ]]; then
    echo "Creating backup of \"$TARGET\" in \"$TARGET.bak\"."
    mv "$TARGET" "$TARGET.bak"
fi

# Prepare new HTML file
TMP_HTML=$(<"$TEMPLATE")

# Replace variables one at a time
for v in ${VARS}; do
    REPL=$(<"$LANGDIR/${v}.txt")
    TMP_HTML=$(echo -e "${TMP_HTML/<%$v%>/$REPL}")
done

# Copy generated HTML to target location
echo "$TMP_HTML" > "$TARGET"
echo "Done!"
exit 0;

# EOF gen_html.sh
